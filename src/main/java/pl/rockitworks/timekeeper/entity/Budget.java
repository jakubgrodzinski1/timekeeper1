package pl.rockitworks.timekeeper.entity;

import javax.persistence.*;

@Entity
@Table(name = "BUDGETS")
public class Budget {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @Column(name = "PROJECT")
    private Project project;

    @Column(name = "REVENUE")
    private double revenue;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }
}
