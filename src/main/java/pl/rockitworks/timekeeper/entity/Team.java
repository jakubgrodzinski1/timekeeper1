package pl.rockitworks.timekeeper.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="TEAM")
public class Team {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long ID;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name="LEADER")
    @ManyToOne
    private User leader;

    @Column(name = "PROJECT", nullable = false)
    @OneToMany
    private List<Project> projectID;

    @Column(name = "MEMBERS")
    @ManyToMany
    private List<User> userID;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getLeader() {
        return leader;
    }

    public void setLeader(User leader) {
        this.leader = leader;
    }

    public List<Project> getProjectID() {
        return projectID;
    }

    public void setProjectID(List<Project> projectID) {
        this.projectID = projectID;
    }

    public List<User> getUserID() {
        return userID;
    }

    public void setUserID(List<User> userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "Team{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", leader=" + leader +
                ", projectID=" + projectID +
                ", userID=" + userID +
                '}';
    }
}
