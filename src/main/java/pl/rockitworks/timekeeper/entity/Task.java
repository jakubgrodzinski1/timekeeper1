package pl.rockitworks.timekeeper.entity;



import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tasks")
public class Task
{
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name="description")
    private String description;
    @ManyToMany
    private List<User> usersWorkingOnTask = new ArrayList<>();
    @ManyToOne
    private Project project;

    @ManyToOne
    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @OneToOne
    private TimeSheet timesheet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getUsersWorkingOnTask() {
        return usersWorkingOnTask;
    }

    public void setUsersWorkingOnTask(List<User> usersWorkingOnTask) {
        this.usersWorkingOnTask = usersWorkingOnTask;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public TimeSheet getTimesheet() {
        return timesheet;
    }

    public void setTimesheet(TimeSheet timesheet) {
        this.timesheet = timesheet;
    }
}
