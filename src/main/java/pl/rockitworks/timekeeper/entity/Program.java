package pl.rockitworks.timekeeper.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PROGRAM")
public class Program {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "CREATED_BY", nullable = false)
    @OneToOne
    @JoinColumn(name = "ID")
    private User createdBy;

    @OneToMany
    @Column(name = "PROJECTS")
    @JoinColumn(name = "ID")
    private List<Project> projects;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return "Program{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createdBy=" + createdBy +
                ", projects=" + projects +
                '}';
    }
}
