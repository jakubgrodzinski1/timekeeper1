package pl.rockitworks.timekeeper.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "timesheets")
public class TimeSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(mappedBy = "task",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Task task;

    @Temporal(TemporalType.DATE)
    @Column(name="start_date")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name="end_date")
    private Date endDate;

    @Column(name="value")

    private int value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public TimeSheet(Long id, Task task, Date startDate, Date endDate, int value) {
        this.id = id;
        this.task = task;
        this.startDate = startDate;
        this.endDate = endDate;
        this.value = value;
    }

    @Override
    public String toString() {
        return "TimeSheet{" +
                "id=" + id +
                ", task=" + task +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", value=" + value +
                '}';
    }
}
